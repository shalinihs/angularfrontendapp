import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { Contact } from './contact';
import {Observable} from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  uri=`${environment.baseuri}/contact`

  constructor(private http:HttpClient) { }
  //1.save contact
  createContact(contact: Contact) :Observable<any>{
    return this.http.post(`${this.uri}/create`,contact,{responseType:`text`,});
  }
 
  //2. fetch all
   fetchAllContact():Observable<Contact[]>{
     return this.http.get<Contact[]>(`${this.uri}/all`);
   }

  //3. fetch one
  fetchOneContact(id:number):Observable<Contact>{
    return this.http.get<Contact>(`${this.uri}/find/${id}`);
  }
  //4. remove 
removeOneContact(id:number){
  return this.http.delete(`${this.uri}/remove/${id}`,{responseType:'text',});
}



  //5.update

  updateOneContact(contact:Contact){
    return this.http.put(`${this.uri}/modify`,contact,{responseType:`text`,});
  }
}
