import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContactComponent } from './contact/contact.component';

import {HttpClientModule} from '@angular/common/http';
import { ContactRegisterComponent } from './contact-register/contact-register.component';
import { ContactAllComponent } from './contact-all/contact-all.component';
import { ContactEditComponent } from './contact-edit/contact-edit.component'

@NgModule({
  declarations: [
    AppComponent,
    ContactComponent,
    ContactRegisterComponent,
    ContactAllComponent,
    ContactEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
