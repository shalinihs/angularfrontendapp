import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Contact } from '../contact';
import { ContactService } from '../contact.service';

@Component({
  selector: 'app-contact-edit',
  templateUrl: './contact-edit.component.html',
  styleUrls: ['./contact-edit.component.less']
})
export class ContactEditComponent implements OnInit {
  contact:Contact=new Contact(0,'','','','');

  constructor(private service:ContactService,
    private activatedRoute:ActivatedRoute,
   private router:Router) { }

  ngOnInit(): void {
    const id=this.activatedRoute.snapshot.params['id'];
    this.service.fetchOneContact(id).subscribe(
      data=>{
        this.contact=data;
      },error=>{
      console.log(error);
      this.router.navigate(['list']);
      }
    );
  }
  updateContact(){
   this.service.updateOneContact(this.contact).subscribe(
     data=>{
       console.log(data);
       this.router.navigate(['list']);

     },error=>{
       console.log(error);

     }
   );
  }

}
