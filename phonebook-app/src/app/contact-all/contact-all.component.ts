import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Contact } from '../contact';
import { ContactService } from '../contact.service';

@Component({
  selector: 'app-contact-all',
  templateUrl: './contact-all.component.html',
  styleUrls: ['./contact-all.component.less']
})
export class ContactAllComponent implements OnInit {
  //Array
  contacts: Contact[] = [];
  message: string = '';
  //Dependency Injection
  constructor(private service: ContactService,private router:Router) { }

  ngOnInit(): void {
    this.fetchAllContact(); //whenever ContactAllComponent loaded this method is called
  }
  fetchAllContact() {
    this.service.fetchAllContact().subscribe(
      data => {              //success
        this.contacts = data;

      }, error => {          //error
        console.log(error);

      }
    );
  }

  deleteContact(id: number) {
    this.service.removeOneContact(id).subscribe(
      data => {
        this.message = data;
        this.fetchAllContact();
      }, error => {
        console.error(error);
      }
    );
  }
  editContact(id:number){
    this.router.navigate(['edit',id]);
    
  }


}
