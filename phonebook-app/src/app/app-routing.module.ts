import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactAllComponent } from './contact-all/contact-all.component';
import { ContactEditComponent } from './contact-edit/contact-edit.component';
import { ContactRegisterComponent } from './contact-register/contact-register.component';

const routes: Routes = [
  {path:'list',component:ContactAllComponent},
  {path:'add',component:ContactRegisterComponent},
  {path:'edit/:id',component:ContactEditComponent},
  {path:'*',redirectTo:'list',pathMatch:'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
