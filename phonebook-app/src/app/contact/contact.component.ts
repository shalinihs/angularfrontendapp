import { Component, OnInit } from '@angular/core';
import { Contact } from '../contact';
import { ContactService } from '../contact.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.less']
})
export class ContactComponent implements OnInit {
  // Form Backing Object
  contact: Contact = new Contact(0, '', '', '', '');
  // Dependency Injection

  constructor(private contactService: ContactService) { }

  ngOnInit(): void {
    console.log("Contact");
    
  }

}
