import { Component, OnInit } from '@angular/core';
import { Contact } from '../contact';
import { ContactService } from '../contact.service';

@Component({
  selector: 'app-contact-register',
  templateUrl: './contact-register.component.html',
  styleUrls: ['./contact-register.component.less']
})
export class ContactRegisterComponent implements OnInit {

  contact:Contact=new Contact(0,'','','','');
  message:string='';
  
  constructor(private service:ContactService) {}

  ngOnInit(): void {
  }
  createContact(){
    this.service.createContact(this.contact)
    .subscribe(data=>{
      this.message=data;
      this.contact=new Contact(0,'','','','');

    },error=>{
      console.log(error);
      this.message='Unable to save ! Contact Admin';

    });

  }

}
