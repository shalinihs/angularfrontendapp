export class Contact {

    constructor(
        public contactId:number,
        public contactName:string,
        public contactNum:string,
        public contactEmail:string,
        public activateSW:string
    ){}
}
