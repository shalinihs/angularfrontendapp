import { Component, OnInit } from '@angular/core';
import { Employee } from '../employee';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {


  // Form Backing Object
  employee:Employee=new Employee(0,'',0,'','','');
  // Dependency Injection
  constructor(private service:EmployeeService) { }

  ngOnInit(): void {
  }

}
