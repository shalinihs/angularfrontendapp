import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from '../employee';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employee-all',
  templateUrl: './employee-all.component.html',
  styleUrls: ['./employee-all.component.css']
})
export class EmployeeAllComponent implements OnInit {
  //array variable
  employees:Employee[]=[];
  message:string='';

  //Dependency Injection
  constructor(private service:EmployeeService,private router:Router) { }

  ngOnInit(): void {
    this.fetchAllEmployees();
  }
//call service method to fetch Data
  fetchAllEmployees(){
    this.service.fetchAllEmployee().subscribe(
      data=>{
        this.employees=data;
        console.log(data);

      },error=>{
        console.log(error);

      }
    );

  }
  // Delete One Employee
  deleteOneEmployee(id:number){
    // alert("Employee "+id+" Deleted");
this.service.removeOneEmployee(id).subscribe(
  (data)=>{
    this.message=data;
    this.fetchAllEmployees();
    
  },error=>{
    console.log(error);
  }
);
}
editEmployee(id:number){
this.router.navigate(['edit',id]);
}


}
